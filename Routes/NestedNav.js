// import React from 'react';
// import SplashScreen from '../Screens/SplashScreen';
// import Login from '../Screens/Login';
// import Calendar from '../Screens/Calendar';
// import RegisterScreen from '../Screens/RegisterScreen';
// import {AuthContext} from '../src/context/AuthContext';
// import Message from '../Screens/Message';
// import Details from '../Screens/Details';
// import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
// import {createDrawerNavigator} from '@react-navigation/drawer';
// import {createNativeStackNavigator} from '@react-navigation/native-stack';

// const Stack = createNativeStackNavigator();
// const Tab = createBottomTabNavigator();
// const Drawer = createDrawerNavigator();

// export default function NestedNav() {
//   const [isLoading, setIsLoading] = React.useState(true);
//   const [userTocken, setUserTocken] = React.useState(null);

//   const authContext = React.useMemo(() => {
//     return {
//       signIn: () => {
//         setIsLoading(false);
//         setUserTocken('asdf');
//       },
//       singUp: () => {
//         setIsLoading(false);
//         setUserTocken('asdf');
//       },
//       singOut: () => {
//         setIsLoading(false);
//         setUserTocken(null);
//       },
//     };
//   }, []);
//   React.useEffect(() => {
//     setTimeout(() => {
//       setIsLoading(false);
//     }, 1000);
//   }, []);
//   if (isLoading) {
//     return <SplashScreen />;
//   }
//   function BottomTab() {
//     return (
//       <BottomTab.Navigator>
//         <Tab.Screen name="calendar" component={Calendar} />
//         <Tab.Screen name="message" component={Message} />
//         <Tab.Screen name="details" component={Details} />
//         <Tab.Screen name="register" component={RegisterScreen} />
//       </BottomTab.Navigator>
//     );
//   }
//   function StackScreen() {
//     return (
//       <Stack.Navigator
//         screenOptions={{headerShown: false}}
//         initialRouteName="login">
//         <Stack.Screen name="login" component={Login} />
//         <Stack.Screen name="calendar2" component={Calendar} />
//         <Stack.Screen name="message2" component={Message} />
//         <Stack.Screen name="details2" component={Details} />
//         <Stack.Screen name="register2" component={RegisterScreen} />
//         {/* <Stack.Screen name="DrawerScreen" component={DrawerScreen} /> */}
//         {/* <Stack.Screen name="BottomTab" component={BottomTab} /> */}
//       </Stack.Navigator>
//     );
//   }
//   function DrawerScreen() {
//     return (
//       <Drawer.Navigator screenOptions={{headerShown: false}}>
//         <Drawer.Screen name="register" component={RegisterScreen} />
//         <Drawer.Screen name="calendar1" component={Calendar} />
//         <Drawer.Screen name="message1" component={Message} />
//         <Drawer.Screen name="details1" component={Details} />
//       </Drawer.Navigator>
//     );
//   }
//   return (
//     <AuthContext.Provider value={authContext}>
//       <Stack.Navigator screenOptions={{headerShown: false}}>
//         <Stack.Screen name="StackScreen" component={StackScreen} />
//         <Stack.Screen name="DrawerScreen" component={DrawerScreen} />
//       </Stack.Navigator>
//     </AuthContext.Provider>
//   );
// }

import React, {useContext} from 'react';
import Login from '../Screens/Login';
import RegisterScreen from '../Screens/RegisterScreen';
import Calendar from '../Screens/Calendar';
import SplashScreen from '../Screens/SplashScreen';
import Message from '../Screens/Message';
import Details from '../Screens/Details';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {AuthContext} from '../src/context/AuthContext';
import LogOut from '../Screens/LogOut';

const AuthStack = createNativeStackNavigator();
const BottomTab = createBottomTabNavigator();
const LoginStack = createNativeStackNavigator();
const CalendarStack = createNativeStackNavigator();
const DetailsStack = createNativeStackNavigator();
const MessageStack = createNativeStackNavigator();
const RegisterScreenStack = createNativeStackNavigator();

const CalendarStackScreen = () => (
  <CalendarStack.Navigator screenOptions={{headerShown: false}}>
    <CalendarStack.Screen name="calendar" component={Calendar} />
  </CalendarStack.Navigator>
);
const DetailsStackScreen = () => (
  <DetailsStack.Navigator screenOptions={{headerShown: false}}>
    <DetailsStack.Screen name="details" component={Details} />
  </DetailsStack.Navigator>
);
const MessageStackScreen = () => (
  <MessageStack.Navigator screenOptions={{headerShown: false}}>
    <MessageStack.Screen name="message" component={Message} />
  </MessageStack.Navigator>
);
const LoginStackScreen = () => (
  <LoginStack.Navigator screenOptions={{headerShown: false}}>
    <LoginStack.Screen name="login" component={Login} />
  </LoginStack.Navigator>
);
const RegisterScreenStackScreen = () => (
  <RegisterScreenStack.Navigator screenOptions={{headerShown: false}}>
    <RegisterScreenStack.Screen name="register" component={RegisterScreen} />
  </RegisterScreenStack.Navigator>
);

const Tabs = () => (
  <BottomTab.Navigator
    screenOptions={{headerShown: false, tabBarShowLabel: false}}
    initialRouteName="login">
    <BottomTab.Screen name="calendar3" component={CalendarStackScreen} />
    <BottomTab.Screen name="message3" component={MessageStackScreen} />
    <BottomTab.Screen
      name="Details3"
      component={DetailsStackScreen}
      options={{tabBarStyle: {display: 'none'}}}
    />
    <BottomTab.Screen
      name="register3"
      component={RegisterScreenStackScreen}
      options={{tabBarStyle: {display: 'none'}}}
    />
  </BottomTab.Navigator>
);
const Drawer = createDrawerNavigator();
const NestedNav = () => {
  const [isLoading, setIsLoading] = React.useState(true);
  const [userTocken, setUserTocken] = React.useState(null);

  const authContext = React.useMemo(() => {
    return {
      signIn: () => {
        setIsLoading(false);
        setUserTocken('asdf');
      },
      signUp: () => {
        setIsLoading(false);
        setUserTocken('asdf');
      },
      signOut: () => {
        setIsLoading(false);
        setUserTocken(null);
      },
    };
  }, []);
  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 1000);
  }, []);
  if (isLoading) {
    return <SplashScreen />;
  }

  return (
    <AuthContext.Provider value={authContext}>
      {userTocken !== null ? (
        <Drawer.Navigator
          screenOptions={{headerShown: false}}
          initialRouteName="login">
          <Drawer.Screen name="Calendar2" component={Tabs} />
          <Drawer.Screen name="message2" component={MessageStackScreen} />
          <Drawer.Screen name="details2" component={DetailsStackScreen} />
          <Drawer.Screen
            name="Logout"
            component={LogOut}
            listeners={({navigation}) => ({
              state: e => {
                if (e.data.state.index === 3) {
                  // 3 is index of logout item in drawer

                  navigation.navigate(LoginStackScreen);
                }
              },
            })}
          />
        </Drawer.Navigator>
      ) : (
        <AuthStack.Navigator
          screenOptions={{headerShown: false}}
          initialRouteName="login">
          <AuthStack.Screen name="login1" component={LoginStackScreen} />
          <AuthStack.Screen name="calendar1" component={CalendarStackScreen} />
          <AuthStack.Screen name="register1" component={RegisterScreen} />
          <AuthStack.Screen name="details1" component={DetailsStackScreen} />
        </AuthStack.Navigator>
      )}
    </AuthContext.Provider>
  );
};

export default NestedNav;

// import React from 'react';
// import {View, Image, useWindowDimensions} from 'react-native';
// import {createNativeStackNavigator} from '@react-navigation/native-stack';
// import Login from '../Screens/Login';
// import Details from '../Screens/Details';
// import Calendar from '../Screens/Calendar';
// import Message from '../Screens/Message';
// import RegisterScreen from '../Screens/RegisterScreen';
// import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
// import {createDrawerNavigator} from '@react-navigation/drawer';
// import {ScreenStack} from 'react-native-screens';
// import SplashScreen from '../Screens/SplashScreen';

// const BottomTab = createBottomTabNavigator();
// const Stack = createNativeStackNavigator();
// const Drawer = createDrawerNavigator();
// const StackScreen = createNativeStackNavigator();

// export default function NestedNav() {
//   function HomeScreen() {
//     return (
//       <BottomTab.Navigator
//         screenOptions={{
//           headerShown: false,
//           tabBarShowLabel: false,
//           tabBarActiveTintColor: '#0284ff',
//         }}
//         initialRouteName={Login}>
//         <BottomTab.Screen
//           options={{
//             tabBarStyle: {display: 'none'},
//             tabBarIcon: ({focused}) =>
//               focused ? (
//                 <View>
//                   <Image
//                     source={require('../Screens/Image/colourman.png')}
//                     resizeMod="contain"
//                     style={{
//                       width: 50,
//                       height: 50,
//                     }}
//                   />
//                 </View>
//               ) : (
//                 <View>
//                   <Image
//                     source={require('../Screens/Image/man.png')}
//                     resizeMod="contain"
//                     style={{
//                       width: 50,
//                       height: 50,
//                     }}
//                   />
//                 </View>
//               ),
//           }}
//           name="login"
//           component={Login}
//         />
//         <BottomTab.Screen
//           options={{
//             tabBarIcon: ({focused}) =>
//               focused ? (
//                 <View>
//                   <Image
//                     source={require('../Screens/Image/calendar.png')}
//                     resizeMod="contain"
//                     style={{
//                       width: 50,
//                       height: 50,
//                     }}
//                   />
//                 </View>
//               ) : (
//                 <View>
//                   <Image
//                     source={require('../Screens/Image/calender.png')}
//                     resizeMod="contain"
//                     style={{
//                       width: 50,
//                       height: 50,
//                     }}
//                   />
//                 </View>
//               ),
//           }}
//           name="calendar"
//           component={Calendar}
//         />
//         <BottomTab.Screen
//           options={{
//             tabBarIcon: ({focused}) =>
//               focused ? (
//                 <View>
//                   <Image
//                     source={require('../Screens/Image/chaticon.png')}
//                     resizeMod="contain"
//                     style={{
//                       width: 50,
//                       height: 50,
//                     }}
//                   />
//                 </View>
//               ) : (
//                 <View>
//                   <Image
//                     source={require('../Screens/Image/chat.png')}
//                     resizeMod="contain"
//                     style={{
//                       width: 50,
//                       height: 50,
//                     }}
//                   />
//                 </View>
//               ),
//           }}
//           name="message"
//           component={Message}
//         />
//         <BottomTab.Screen
//           options={{
//             tabBarStyle: {display: 'none'},
//             tabBarIcon: ({focused}) =>
//               focused ? (
//                 <View>
//                   <Image
//                     source={require('../Screens/Image/pad.png')}
//                     resizeMod="contain"
//                     style={{
//                       width: 50,
//                       height: 50,
//                     }}
//                   />
//                 </View>
//               ) : (
//                 <View>
//                   <Image
//                     source={require('../Screens/Image/notepad.png')}
//                     resizeMod="contain"
//                     style={{
//                       width: 50,
//                       height: 50,
//                     }}
//                   />
//                 </View>
//               ),
//           }}
//           name="details"
//           component={Details}
//         />
//       </BottomTab.Navigator>
//     );
//   }

//   function Editable() {
//     return (
//       <StackScreen.Navigator
//         initialRouteName={SplashScreen}
//         screenOptions={{headerShown: false}}>
//         <StackScreen.Screen name="SplashScreen" component={SplashScreen} />
//         <StackScreen.Screen name="login" component={Login} />
//         <StackScreen.Screen name="message" component={Message} />
//         <StackScreen.Screen name="details" component={Details} />
//         <StackScreen.Screen name="register" component={RegisterScreen} />
//       </StackScreen.Navigator>
//     );
//   }

//   function CustomDrawer() {
//     return (
//       <Drawer.Navigator
//         initialRouteName={Login}
//         screenOptions={{headerShown: false}}>
//         <Drawer.Screen name="login" component={Login} />
//         <Drawer.Screen name="message" component={Message} />
//         <Drawer.Screen name="details" component={Details} />
//         <Drawer.Screen name="calendar" component={Calendar} />
//       </Drawer.Navigator>
//     );
//   }

//   return (
//     <Stack.Navigator
//       initialRouteName={SplashScreen}
//       screenOptions={{headerShown: false}}>
//       <Stack.Screen name="HomeScreen" component={HomeScreen} />
//       <Stack.Screen name="CustomDrawer" component={CustomDrawer} />
//       <Stack.Screen name="SplashScreen" component={SplashScreen} />
//       {/* <Stack.Screen name="login" component={Login} />
//       <Stack.Screen name="calendar" component={Calendar} />
//       <Stack.Screen name="message" component={Message} />
//       <Stack.Screen name="Details" component={Details} /> */}
//       <Stack.Screen name="register" component={RegisterScreen} />
//       <Stack.Screen name="editable" component={Editable} />
//     </Stack.Navigator>
//   );
// }
