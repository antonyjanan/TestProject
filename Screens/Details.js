import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

const Details = ({navigation}) => {
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.header}>
          <ImageBackground
            source={require('./Image/gradient.jpg')}
            style={styles.bgImg}>
            <View style={styles.parent}>
              <View style={styles.headerChildOne}>
                <Image
                  source={require('./Image/a.png')}
                  style={styles.imageclub}
                />
                <Text style={styles.fcm}>FCM U13</Text>
              </View>
              <View style={styles.verticalLine} />
              <View style={styles.headerChildTwo}>
                <Image source={require('./Image/shield.png')} />
                <Text style={styles.grans}>Grans</Text>
              </View>
            </View>
            <View style={styles.middle}>
              <View style={styles.firstBox}>
                <Text style={styles.infos}>INFOS</Text>
              </View>
              <View style={styles.secondBox}>
                <Text>PLAYERS</Text>
              </View>
              <View style={styles.thirdBox}>
                <Text>COMPOSITION</Text>
              </View>
              <View style={styles.fourthBox}>
                <Text>MESSAGE</Text>
              </View>
            </View>
          </ImageBackground>
        </View>
        <View style={styles.body}>
          <View style={styles.bg1}>
            <Image source={require('./Image/pad.png')} style={styles.img1} />
            <Text style={styles.text1}>Championship match</Text>
          </View>
          <View style={styles.bg1}>
            <Image
              source={require('./Image/location.png')}
              style={styles.img2}
            />
            <Text style={styles.text1}>
              Parc de Loisirs de la Fontaine Mary Rose,Grans,Provence-Alpes-Côte
              d'Azur, 13450
            </Text>
          </View>
          <View style={styles.bg1}>
            <Image
              source={require('./Image/calendar.png')}
              style={styles.img3}
            />
            <Text style={styles.text1}>19 Sep 2018 20:40</Text>
          </View>
          <View style={styles.bg1}>
            <Image source={require('./Image/clock.png')} style={styles.img4} />
            <Text style={styles.text1}>19:40</Text>
          </View>
        </View>
        <View style={styles.footer}>
          <TouchableOpacity
            onPress={() => navigation.navigate('login')}
            style={styles.button}>
            <Text style={styles.stadium}>STADIUM ADDRESS</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  parent: {
    margin: 30,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  imageclub: {
    borderRadius: 100,
    width: 60,
    height: 60,
  },
  grans: {
    left: 15,
  },
  fcm: {
    top: 7,
  },
  verticalLine: {
    marginTop: 15,
    height: 40,
    width: 1.5,
    backgroundColor: 'white',
  },
  middle: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    height: 40,
    top: 20,
  },
  firstBox: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 50,
    elevation: 30,
    backgroundColor: 'white',
    borderRadius: 10,
  },
  infos: {
    color: '#3480FF',
  },
  secondBox: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 70,
    elevation: 30,
    backgroundColor: 'white',
    borderRadius: 10,
  },
  thirdBox: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 100,
    elevation: 30,
    backgroundColor: 'white',
    borderRadius: 10,
  },
  fourthBox: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 80,
    elevation: 30,
    backgroundColor: 'white',
    borderRadius: 10,
  },
  body: {
    height: 400,
    margin: 30,
    flexDirection: 'column',
    justifyContent: 'space-evenly',
  },
  bg1: {
    flexDirection: 'row',
  },
  img1: {
    width: 40,
    height: 40,
  },
  img2: {
    width: 40,
    height: 40,
  },
  img3: {
    width: 40,
    height: 40,
  },
  img4: {
    width: 40,
    height: 40,
  },
  text1: {
    marginLeft: 25,
    fontSize: 18,
  },
  footer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    backgroundColor: '#3480FF',
    width: 250,
    height: 45,
    borderRadius: 10,
  },
  stadium: {
    color: 'white',
    marginTop: 10,
    textAlign: 'center',
  },
});
export default Details;
