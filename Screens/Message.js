import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
} from 'react-native';

const Message = ({navigation}) => {
  const array = [
    {
      key: '1',
      image: require('./Image/a.png'),
      title: 'Match',
      subtitle: 'Opponent:Test',
      subtitletext: 'Summons pending',
      text: 'Waiting man of the match',
      date: '12 Sep 2018 15:00',
    },
    {
      key: '2',
      image: require('./Image/a.png'),
      title: 'Match',
      subtitle: 'Opponent:Test',
      subtitletext: 'Summons pending',
      text: 'Waiting man of the match',
      date: '17 Sep 2018 15:00',
    },
    {
      key: '3',
      image: require('./Image/a.png'),
      title: 'Entertainment',
      subtitle: 'Event Title:Celebration',
      subtitletext: 'Summons pending',
      text: 'Waiting man of the match',
      date: '17 Sep 2018 21:00',
    },
    {
      key: '4',
      image: require('./Image/a.png'),
      title: 'Match',
      subtitle: 'Opponent: grans',
      subtitletext: 'Summons pending',
      text: 'Waiting man of the match',
      date: '17 Sep 2018 21:00',
    },
  ];
  const list = () => {
    return array.map(element => {
      return (
        <View key={element.key} style={styles.mainArrCon}>
          <View style={styles.arrcontainer}>
            <Image source={element.image} style={styles.helloImg} />
            <View style={styles.arr}>
              <View style={styles.arr1}>
                <Text style={styles.letter}>{element.title}</Text>
              </View>
              <View style={styles.arr2}>
                <Text style={styles.letter}>{element.subtitle}</Text>
              </View>
              <View style={styles.arr3}>
                <Text style={styles.letter}>{element.subtitletext}</Text>
              </View>
              <View style={styles.arr4}>
                <Text style={styles.letter}>{element.text}</Text>
              </View>
              <View style={styles.arr5}>
                <Text style={styles.letter}>{element.date}</Text>
              </View>
            </View>
          </View>
        </View>
      );
    });
  };
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <ImageBackground
          source={require('./Image/gradient.jpg')}
          style={styles.imgBg}>
          <View style={styles.innerHeader}>
            <View style={styles.innerImg}>
              <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
                <Image
                  source={require('./Image/menu.png')}
                  style={styles.menu}
                />
              </TouchableOpacity>
            </View>
            <View style={styles.innerText}>
              <Text style={styles.message}>Message</Text>
            </View>
          </View>
        </ImageBackground>
      </View>
      <ScrollView>
        <View style={styles.column}>
          <View style={styles.innerColumn}>
            <Text style={styles.convo}>YOU HAVE PENDING CONVOCATION</Text>
          </View>
        </View>
        <View style={styles.body}>
          <View style={styles.childBody}>{list()}</View>
        </View>
      </ScrollView>
      {/* <View style={styles.footer}>
        <TouchableOpacity onPress={() => navigation.navigate('calendar')}>
          <Image source={require('./Image/calender.png')} style={styles.img} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('message')}>
          <Image source={require('./Image/chaticon.png')} style={styles.img} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('login')}>
          <Image source={require('./Image/man.png')} style={styles.img} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('details')}>
          <Image source={require('./Image/notepad.png')} style={styles.img} />
        </TouchableOpacity>
      </View> */}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imgBg: {
    height: 90,
  },
  innerHeader: {
    flexDirection: 'row',
  },
  column: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: 15,
  },
  innerColumn: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 300,
    height: 40,
    elevation: 30,
    backgroundColor: 'white',
  },
  convo: {
    color: 'red',
    fontWeight: '600',
  },
  menu: {
    margin: 20,
    height: 35,
    width: 35,
  },
  innerText: {
    marginLeft: 90,
    justifyContent: 'center',
  },
  letter: {
    fontSize: 16,
  },
  message: {
    fontSize: 20,
    color: 'white',
  },
  mainArrCon: {
    borderRadius: 5,
    elevation: 10,
    margin: 10,
    backgroundColor: 'white',
  },
  helloImg: {
    height: 100,
    width: 100,
    borderRadius: 100,
  },
  footer: {
    margin: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  arrcontainer: {
    flexDirection: 'row',
  },
  img: {
    height: 50,
    width: 50,
  },
});
export default Message;
