import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Dimensions,
  TextInput,
  Image,
  ScrollView,
  StatusBar,
  Pressable,
} from 'react-native';
import React from 'react';

const RegisterScreen = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView>
        <StatusBar backgroundColor="#edf3fb" barStyle="dark-content" />
        <View style={styles.header}>
          <Image source={require('./Image/a.png')} style={styles.Brand} />
        </View>
        <View style={styles.footer}>
          <View style={styles.inputContainer}>
            <TextInput
              placeholder="Name"
              style={styles.Name}
              autoCapitalize="none"
            />
            <TextInput
              placeholder="New Email ID"
              style={styles.emailInputStyle}
              autoCapitalize="none"
            />
            <TextInput
              secureTextEntry={true}
              placeholder="New password"
              style={styles.passwordInputStyle}
            />
            <Pressable
              style={styles.button}
              onPress={() => navigation.navigate('login')}>
              <Text style={styles.login}>Login</Text>
            </Pressable>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
const {height} = Dimensions.get('screen');
const Brand = height * 0.23;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#edf3fb',
  },
  header: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    flex: 1,
    backgroundColor: 'white',
    borderTopLeftRadius: 30,
    borderRadius: 30,
    paddingVertical: 155,
    paddingHorizontal: 30,
    elevation: 35,
    marginHorizontal: 30,
  },
  Brand: {
    width: Brand,
    height: Brand,
  },
  button: {
    width: '100%',
    height: 45,
    borderRadius: 20,
    backgroundColor: '#4e6bc2',
    alignItems: 'center',
    marginTop: 20,
  },
  Name: {
    borderRadius: 15,
    marginBottom: 12,
    paddingLeft: 10,
    borderWidth: 1.5,
    borderColor: 'grey',
  },
  emailInputStyle: {
    borderRadius: 15,
    paddingLeft: 10,
    borderWidth: 1.5,
    borderColor: 'grey',
  },
  passwordInputStyle: {
    marginTop: 12,
    marginBottom: 12,
    borderRadius: 15,
    paddingLeft: 10,
    borderWidth: 1.5,
    borderColor: 'grey',
  },
  login: {
    color: 'white',
    paddingTop: 8,
    fontSize: 18,
    fontWeight: 'bold',
  },
});

export default RegisterScreen;
